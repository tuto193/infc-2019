;
; task10.asm
;
; Created: 23/01/2020 12:40:17
; Author : carlos
;
.def    sec0 = R16        ;the seconds' counter
.def    sec1 = R17        ;the tens' counter
.def    min0 = R18        ;the minute counter
.def    time = R19        ;the timer that counts down
.def    tmp = R20       ;a temporary general purpose var
.def    total = R21      ;a second tmp
.def    XL = R26        ; our X
.def    XH = R27        ;
.def    ZL = R30        ;
.def    ZH = R31        ;

.equ    EVENTS = 1     ; EVENTS sets the frequenzy of the whole (1:1 relationship)

.DSEG
.org    $0100
Timer_Values: .BYTE 12  ;The values that the timer can count to are kept here

.CSEG
.org    $0100
DATA: .db 150, 160, 170, 180, 180, 190, 200, 210, 220, 230, 240, 250

.org    $0              JMP START        ;our program starts in the main
.org    INT0Addr        JMP EXT_ISR0    ; External Int. 0 (configure clock)
.org    INT1Addr        JMP EXT_ISR1    ; External Int. 1(Output to Display)
.org    OC0Addr         JMP ISR_TC0     ; T/C0 Routine: Handle our clock

.org    INT_VECTORS_SIZE
##############################################################
;Use the values that are in time (actual amount of seconds)
;an sets:
;   min0(minutes) = result from "time/60" (int division)
;   sek1(tens)    = rest from "time/60" (int division)
;   sek0(seconds) = time % 10
CONFIGURE_DISPLAY:
    PUSH    tmp
    IN      tmp, SREG
    PUSH    tmp
    PUSH    time
    CLR     sec0
    CLR     sec1
    CLR     min0
    SUBTRACT60:
        SUBI    time, 60
        BRCS    DONE60
        INC     min0
        JMP     SUBTRACT60
    DONE60:
        SUBI    time, -60   ;the remaining time are the last 60 seconds
    SUBTRACT10:
        SUBI    time, 10
        BRCS    DONE10
        INC     sec1
        JMP     SUBTRACT10
    DONE10:
        SUBI    time, -10
        MOV     sec0, time

    POP     time
    POP     tmp
    OUT     SREG, tmp
    POP     tmp
    RET

; (Nicht Seiteneffekt-freies) Unterprogramm zur Initialisierung des Arrays
; Bei Aufruf sollte beachtet werden, dass Z, X und R16 ver�ndert werden.
; L�dt die konstanten Werte von CONSTANTS in das DATA Array.
INITARRAY:
	; Lade Adresse der Constanten in Z
	LDI ZL, low(DATA<<1)   ; Links-Shift bei Adressierung des Programmspeichers notwendig
	LDI ZH, high(DATA<<1)  ; Adresse kann au�erdem NUR in Z stehen
	; Lade Adresse des Arrays in X (es h�tte auch Y oder Z verwendet werden k�nnen)
	LDI XL, low(Timer_Values)
	LDI XH, high(Timer_Values)
    IALoop:
        LPM tmp, Z+        ; Lade eine Konstante
        ST X+, tmp         ; und speichere sie im Array
        CPI XL, low(Timer_Values+12) ; Vergleich mit der End-Adresse des Arrays
        BRNE IALoop         ; Mache weiter, solange noch nicht am Ende angekommen
        RET
;Initialize the Stack Pointer
INIT_SP:
    LDI tmp, low(RAMEND)    ;initialize SP to end of memeory
    OUT SPL, tmp
    LDI tmp, high(RAMEND)
    OUT SPH, tmp
    RET

;Initialize all ports making sure that PULL-UP is enabled (POSTX=1)
INIT_PORTS:
    ;-------------set all INPUT-Ports-------------------
    ;PORTA are all input
    CBI     DDRA, PA0
    CBI     DDRA, PA1
    CBI     DDRA, PA2
    CBI     DDRA, PA3
    CBI     DDRA, PA4
    CBI     DDRA, PA5
    CBI     DDRA, PA6
    CBI     DDRA, PA7
    SBI     PORTA, PA0
    SBI     PORTA, PA1
    SBI     PORTA, PA2
    SBI     PORTA, PA3
    SBI     PORTA, PA4
    SBI     PORTA, PA5
    SBI     PORTA, PA6
    SBI     PORTA, PA7
    ;PORTB just PB0 as external clock signal
    CBI     DDRB, PB0
    SBI     PORTB, PB0
    ;PORTD  PD1 is the alarm
    ;       PD2 is used to start the whole thing
    ;       PD3 is another external clock signal
    SBI     DDRD, PD1
    CBI     PORTD, PD1
    CBI     DDRD, PD2
    SBI     PORTD, PD2
    CBI     DDRD, PD3
    SBI     PORTD, PD3
    ;--------------------------------------------------
    ;===========set all OUTPUT-Ports====================
    ;PORTC is our seconds-display
    SBI     DDRC, PC0
    SBI     PORTC, PC0
    SBI     DDRC, PC1
    SBI     PORTC, PC1
    SBI     DDRC, PC2
    SBI     PORTC, PC2
    SBI     DDRC, PC3
    SBI     PORTC, PC3
    SBI     DDRC, PC4
    SBI     PORTC, PC4
    SBI     DDRC, PC5
    SBI     PORTC, PC5
    SBI     DDRC, PC6
    SBI     PORTC, PC6
    SBI     DDRC, PC7
    SBI     PORTC, PC7
    ;PORTD (4-7) is our minutes-display
    SBI     DDRD, PD4
    SBI     DDRD, PD5
    SBI     DDRD, PD6
    SBI     DDRD, PD7
    SBI     PORTD, PD4
    SBI     PORTD, PD5
    SBI     PORTD, PD6
    SBI     PORTD, PD7
    ;====================================================
    RET

;initialize the timer and all necessary stuff related to it
INIT_TC0:
    PUSH    tmp             ;save the initial state of tmp
    ;Like in the slides 133
    ; IN      tmp, TIMSK      ;set local interrupt
    ; ORI     tmp, (1<<OCIE0) ;enable bit only
    ; OUT     TIMSK, tmp      ;

    ;Timer/Counter Control Register 0 like Skudes 117
    ;The timer doesn't start yet (CS0X = 0)
    IN      tmp, (0<<CS02)|(0<<CS01)|(0<<CS00)|(1<<WGM01)|(0<<WGM00)
    OUT     TCCR0, tmp      ;Mode: ext. event counter T0 L/H
    LDI     tmp, EVENTS
    OUT     OCR0, tmp       ;Load the register to compare if events have passed
    IN      tmp, TIMSK
    ORI     tmp, (1<<OCIE0) ;Out.Comp.match Int.Enable
    OUT     TIMSK, tmp
    POP     tmp             ;restore initial value of tmp
    RET

;This just counts up every second
ISR_TC0:
	PUSH    tmp; Retten der Register
	IN      tmp, SREG       ; Auch das Statusregister muss gerettet werden
	PUSH    tmp
    ;==========================================
	INC     time                ; Sekunden um 1 hochz�hlen
    LDI     tmp, total
    CP      tmp, time
    BRSH    Carry_On_Normally   ;if we already reached our time
    ;then we need to stop counting, and set the alarm
    OUT     PORTD, PD1          ;activate the alarm
    RCALL   INIT_TC0            ;re-initialize the timer, so that it stops
    LDI     sec0, 0             ; and the values of the display and counter
    LDI     sec1, 0
    LDI     min0, 0
    LDI     total, 0
    LDI     time, 0
    LDI     tmp, 0
    OUT     TCNT0, tmp
    ;==========================================
    Carry_On_Normally:
        POP     tmp
        OUT     SREG, tmp
        POP     tmp
        SEI
        RETI
;Initialize and activate IRS for the INT0 (THE CLOCK)
EXT_ISR0:
    PUSH    tmp             ;save status from tmp
    IN      tmp, SREG       ;and the SREG's
    PUSH    tmp
    IN      tmp, TCCR0      ;
    ORI     tmp, (0<<CS02)|(1<<CS01)|(0<<CS00)
    OUT     TCCR0, tmp      ; Timer wird bei rising edge auf PB0 inkrementiert
    LDI     sec0, 0         ; Sekunden fur den Start auf 0 setzen
    LDI     sec1, 0
    LDI     tmp, 0
    RCALL   SET_TIMER       ; set the total of seconds it will count to
    OUT     TCNT0, tmp      ; Timer auf 0 initialisieren
    POP     tmp
    OUT     SREG, tmp
    POP     tmp
	SEI                     ; Interrupts wieder aktivieren
    RETI

;ISR for EXT_ISR0, which sets up the cooking time and timer depending on
;config settings. If both settings have not been chose, do nothing
INIT_EXT_ISR0:
    PUSH    tmp
    IN      tmp, MCUCR      ;
    ORI     tmp, (1<<ISC00)|(1<<ISC01)
    OUT     MCUCR, tmp                  ;ISC00 & -01 = 1 => Rising edge
    IN      tmp, GICR                   ;get GICR state
    ORI     tmp, (1<<INT0)
    OUT     GICR, tmp                   ;enable INT0 locally
    POP     tmp             ;restore tmp's state
    RET

;Another ISR for our Interrupt 1
EXT_ISR1:
    PUSH    tmp                     ;save state
    IN      tmp, SREG
    PUSH    tmp
    PUSH    sec0
    ;_________________________________________________________
    CALL    CONFIGURE_DISPLAY       ;set the proper values for

    LDI     tmp, sec1               ;load the 10s on the lower side
    SWAP    tmp                     ;move them to the top side
    ANDI    tmp, $F0                ;make sure we don't keep any signs
    ANDI    sec0, $0F
    OR      tmp, sec0               ;on the lower bits, we keep the seconds counter
    OUT     PORTC, tmp              ;output the seconds
    LDI     tmp, min0
    SWAP    tmp                     ;we want to make sure that we output
    ANDI    tmp, $F0                ;through top nibble of pins
    OUT     PORTD, tmp              ;output the minutes
    ;`````````````````````````````````````````````````````````
    POP     sec0
    POP     tmp                     ;restore state
    OUT     SREG, tmp
    POP     tmp
    RETI

;ISR for EXT_ISR1: functions exactly like INIT_EXT_ISR0, but initializes the
;-11 and -10 and INT1
INIT_EXT_ISR1:
    PUSH    tmp
    IN      tmp, MCUCR
    ORI     tmp, (1<<ISC10)|(1<<ISC11)
    OUT     MCUCR, tmp
    IN      tmp, GICR
    ORI     tmp, (1<<INT1)
    OUT     GICR, tmp
    POP     tmp
    RET

;Set the value for the timer's "total" seconds to count
SET_TIMER:
    PUSH    tmp
    PUSH    time
    CLR     time
    ;choose a mode
    SER     tmp
    SBIC    PINA, PA0
    LDI     tmp, 0
    SBIC    PINA, PA1
    LDI     tmp, 1
    SBIC    PINA, PA2
    LDI     tmp, 2

    ;choose an amount of eggs
    SER     total
    SBIC    PINA, PA3
    LDI     total, 0
    SBIC    PINA, PA4
    LDI     total, 1
    SBIC    PINA, PA5
    LDI     total, 2
    SBIC    PINA, PA6
    LDI     total, 3
    ;get the proper index in the array of Timer_Values
    PUSH    total                       ;if tmp or total weren't set
    OR      total, tmp                  ;
    SBRS    total, $FF
    JMP     Set_Timer_Value             ;if total was set, we can ignore it
    ;we should leave the values unchanged, since there were no valid inputs
    POP     total
    POP     tmp
    POP     time
    CLR     total
    RET

    Set_Timer_Value:
        POP     total
        RCAL    Get_Index_Choice
        RCAL    Get_Index_Amount            ;
        MOV     XL, time
        LDI     XH, 0
        SUBI    XL, low(-(Timer_Values))
        SBCI    XH, high(-(Timer_Values))
        ;finally load the values into time
        LD      total, X
        POP     tmp
        POP     time
        RET
        ;set the index of time to tmp * 4
        Get_Index_Choice:
            SBRC    tmp
            RET
            INC     time
            INC     time
            INC     time
            INC     time
            DEC     tmp
            RJMP    Get_Index_Choice

        ; add the amount of eggs (temp2-1) to the index of time
        Get_Index_Amount:
            SBRC    total
            RET
            INC     time
            DEC     total
            RJMP    Get_Index_Amount

; Show the Minutes and Seconds remaining on the timer (min0 : sec1 sec0)
DISPLAY_TIMER:
    RET

;Initialize everything, and start the main loop
START:
    ;initialize background stuff    |
    RCALL   INIT_SP
    CALL    INITARRAY
    ;Initialize all our interrupt-related shit
    RCALL   INIT_PORTS
    RCALL   INIT_EXT_ISR0
    RCALL   INIT_EXT_ISR1
    RCALL   INIT_TC0
    CLR     time
    CLR     tmp
    CLR     sec0
    CLR     sec1
    CLR     min0
    CLR     total
    MAIN_LOOP:
    ; It should work properly without needing to call anything else

    RJMP    MAIN_LOOP