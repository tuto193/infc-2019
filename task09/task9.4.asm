;REGISTER DEFINITIONS
.def THIS = R16 ; Current n
.def RESULT = R26 ; The fac result
.def TMP = R18

; CONSTANTS
.equ N = 3
.equ B = 1

.DSEG ; DATA SEGMENT
.ORG $0100
P: .BYTE 1

.CSEG ; CODE SEGMENT
.ORG $0000 ; Starting Adress

; MAIN PROGRAM
; Computes Factorial
INIT: LDI TMP, high(RAMEND)
	OUT SPH, TMP ; Init Stackpointer
	LDI TMP, low(RAMEND)
	OUT SPL, TMP ; to end of RAM

FACTORIAL: LDI RESULT, 0
	LDI THIS, N 

	SUB N 6 ; Test if n>6
	TST N
	ADD N 6
	BREQ INIT ; Done, Endless loop

	LDI RESULT 1 ;If n valid result needs to be 1
	CALL FAC_BEGIN; for FAC_BEGIN
	RJMP INIT ; Done, Endless loop


FAC_BEGIN:
	PUSH TMP ; Save R22 on Stack
	IN TMP, SREG ; Copy SREG to TMP
	PUSH TMP ; Save TMP on Stack

	PUSH THIS
 
	

	
	DEC THIS
	TST THIS	; If zero goto fac_end
	BREQ FAC_END 
	CALL FAC_BEGIN; Otherwise add recursion

FAC_END:
	POP THIS
	MUL RESULT, THIS ; !!!This line does not save data in result


	POP TMP ; Restore TMP (SREG)
	OUT SREG, TMP ; Copy TMP to SREG
	POP TMP ; Restore TMP (original)
	RET
