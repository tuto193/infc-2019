;
; task09.asm
;
; Created: 15/01/2020 14:56:55
; Author : carlos
;

; REGISTER DEFINITIONS (like in the lecture's)
.def	TEMP	= R16				;Temporary variable
.def	A 		= R18				;Temporary variable
.def	B 		= R20				;Temporary variable
.def    I       = R22               ;Index keeper
.def	ZL		= R30				; Z-Low
.def	ZH		= R31				; Z-High

.DSEG
.ORG	$0100						; The array must start at this address
ARRAY:	.BYTE	36					;Our array can contain UP-TO 36 Elements

.CSEG
.ORG	$0000						;#########################################
INIT:	LDI		ZL, low(ARRAY)		;Initialize all values
		LDI		ZH, high(ARRAY)		;
        LDI     TEMP, high(RAMEND)   ; init the SP to the end of RAM
        OUT     SPH, TEMP
        LDI     TEMP, low(RAMEND)
        OUT     SPL,TEMP
		CLR		TEMP				;
		CLR		B					; B will serve as the reset counter
		CLR		A					; A will be the even number to be moved
		CLR		I					; I will serve as the index counter
;#######################
;Set all values saved in the array to 0
SETARR:	ST		Z+, TEMP			;
		CPI		ZL, low(ARRAY+7)	;
		BRNE	SETARR				;
;#######################
;####################### TEST CODE ###################
; Manually initialize values inside the array
MINIT:
    	LDI		ZL, low(ARRAY)
		LDI		ZH, high(ARRAY)
        LDI		TEMP, $03
		STS		ARRAY, TEMP
		LDI		TEMP, $04
		STS		ARRAY+1, TEMP
		LDI		TEMP, $06
		STS		ARRAY+2, TEMP
		LDI		TEMP, $05
		STS		ARRAY+3, TEMP
		LDI		TEMP, $07
		STS		ARRAY+4, TEMP
		LDI		TEMP, $02
		STS		ARRAY+5, TEMP
		CLR		TEMP
		RJMP	START

;####################### TEST CODE ###################
JUMPEND:
		JMP	END

CHECKODD:
        CLR     B
		POP		TEMP                   ; we haven't moved the odd number
        PUSH    TEMP                ; save the value of TEMP
        ANDI    TEMP, 1             ; if the lowest bit in A is set
        TST     TEMP                ; if A's first bit wasn't set, ignore
        BREQ    RESET               ; -> get back to work on the next index

; TEMP was an ODD number, so we need to find the next even number and switch places with it
MOVEODD:                            ; Switch places of this odd number with
                                    ; the next Even one found
        INC     I                   ; We will check the next index too
        INC     B                   ; how far up we are taking the odd (at least one step up)
        LD      A, Z
        TST     A
        BREQ    JUMPEND             ; the odd was already the last number there was
        PUSH    A
        ANDI    A, 1                ; check if A is also odd A=1 if ODD
        TST     A                   ; Z(A)=0 if A was EVEN
        BREQ    SWITCH              ; Z=0, so we switch their places

; If Z=1, then A was an ODD number, so we just carry on with the next number on the array
RESETMOVE:
		LD		A, Z+
        POP     A
        RJMP    MOVEODD

; If Z=0, then A was an EVEN number, so we switch places with it, since Even numbers should
; stand before the Odd ones
SWITCH:
		MOV     ZL, I
        LDI     ZH, 0
        SUBI    ZL, low(-(ARRAY))
        SBCI    ZH, high(-(ARRAY))
		POP     TEMP				; get TEMP back (Our ODD)
        ST      Z, TEMP             ; Put TEMP here (in front where it belongs)
        PUSH    I
        SUB     I,B                 ; get the lower index to swap
        MOV     ZL, I
        LDI     ZH, 0
        SUBI    ZL, low(-(ARRAY))
        SBCI    ZH, high(-(ARRAY))
        POP     A                   ; get A's original value (EVEN)
        ST		Z, A
        LD      A, Z               ; store the value where TEMP was
        POP     I                   ; make sure we carry on counting properly
        MOV     ZL, I
        LDI     ZH, 0
        SUBI    ZL, low(-(ARRAY))
        SBCI    ZH, high(-(ARRAY))
        PUSH    TEMP
		DEC		I
        RJMP    RESET               ; get back to checking the array


RESET:                              ; Called if we want to POP a back out
        POP     TEMP                ; before carrying on with the checks
        INC     I                   ; We will check the next index too
        ; INC     X                 ; make sure we start from next index inside the array

; Replace with your application code
START:
		CLR		TEMP				; make sure we are working from the beginning
		LD		TEMP, Z+
		PUSH	TEMP				; Check the value we have at X
		TST 	TEMP				;is the value at X already 0?
		BREQ	JUMPEND2			;then just end the program, we are done
        CPI     ZL, low(ARRAY+36)   ; did we already count to the very bottom
        BREQ    JUMPEND2            ; there is nothing more to check. Done
		RJMP    CHECKODD

JUMPEND2:
		JMP		END

.ORG    $1000
END:
    RJMP    END